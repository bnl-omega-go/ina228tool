# Standalone tool for the configuration of INA228 IC via I2C

This tool allows to read a CSV Configuration file generated using Silab Clocking tool and upload it to the ASIC via I2C bus. 
The go tools must be installed on the machine where the compilation will take place. 

## Compilation

To compile : 

```
go mod tidy
go build INA228Tool.go 
```

To compile for a different platform, most likely arm64/linux : 

```
go mod tidy 
GOOS=linux GOARCH=arm64 go build INA228Tool.go
```

The produced executable can be transfered to your arm platform for usage. 

## Usage 

You can use the -h option of the executable to see the available parameters : 

```
./INA228Tool -h
Usage of ./INA228Tool:
  -a uint
    	I2C address of the device (default 104)
  -b string
    	bus to be used for I2C communication ex: /dev/i2c-0,AXI etc. (default "/dev/i2c-2")
  -bov float
    	Bus Over Voltage (V) (default -1)
  -buv float
    	Bus Under Voltage (V) (default -1)
  -c float
    	Max Expected current (A) (for LSB determination) (default 4)
  -dry
    	Dry run reading CSV file
  -p	Set alert pin polarity to 1
  -pm float
    	Max Power (W) (for alert) (default -1)
  -r	Perform Reset
  -rbl
    	Readback limits
  -s float
    	Shunt resistor value (Ω) (default 0.005)
  -sov float
    	Shunt Over Voltage (V) (default -1)
  -suv float
    	Shunt Under Voltage (V) (default -1)
  -tm float
    	Max Temperature (°C) (default -1)
  -v	print extra verbosity during execution
  ```


## Precompiled binaries 

You can always download the precompiled binaries from the artifacts produced during CI  : 

* [amd64](https://gitlab.cern.ch/bnl-omega-go/ina228tool/-/jobs/artifacts/master/raw/INA228Tool?job=build-job)
* [arm32](https://gitlab.cern.ch/bnl-omega-go/ina228tool/-/jobs/artifacts/master/raw/INA228Tool_arm32?job=build_arm32-job)
* [arm64](https://gitlab.cern.ch/bnl-omega-go/ina228tool/-/jobs/artifacts/master/raw/INA228Tool_arm64?job=build_arm64-job)


