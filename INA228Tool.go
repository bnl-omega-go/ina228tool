//go:build linux || darwin
// +build linux darwin

package main

import (
	"flag"
	"fmt"
	"log"

	SlowControl "gitlab.cern.ch/bnl-omega-go/slowcontrol"
)

func main() {

	i2cbus := flag.String("b", "/dev/i2c-3", "bus to be used for I2C communication ex: /dev/i2c-0,AXI etc.")
	devaddr := flag.Uint64("a", 0x68, "I2C address of the device")
	shunt := flag.Float64("s", 0.005, "Shunt resistor value (Ω)")
	maxcurrent := flag.Float64("c", 4, "Max Expected current (A) (for LSB determination)")
	dryrun := flag.Bool("dry", false, "Dry run reading CSV file")
	verbose := flag.Bool("v", false, "print extra verbosity during execution")
	sov := flag.Float64("sov", -1, "Shunt Over Voltage (V)")
	suv := flag.Float64("suv", -1, "Shunt Under Voltage (V)")
	bov := flag.Float64("bov", -1, "Bus Over Voltage (V)")
	buv := flag.Float64("buv", -1, "Bus Under Voltage (V)")
	maxtemp := flag.Float64("tm", -1, "Max Temperature (°C)")
	maxpower := flag.Float64("pm", -1, "Max Power (W) (for alert)")
	reset := flag.Bool("r", false, "Perform Reset")
	pol := flag.Bool("p", false, "Set alert pin polarity to 1")
	rbl := flag.Bool("rbl", false, "Readback limits")

	flag.Parse()

	if *dryrun {
		log.Println("WARNING!!! This is a dry-run !!")
	}

	ctrl := SlowControl.NewINA228Controller(*dryrun, *verbose)
	ctrl.AddDevice("INA228", *i2cbus, uint16(*devaddr))

	// width, _, _ := terminal.GetSize(0)
	id := ctrl.GetID("INA228")
	// widthln := (width - len(id) - 10) / 2
	// line := strings.Repeat("-", widthln)
	fmt.Printf("INA228: %v \n", id)

	if *reset {
		ctrl.Reset("INA228")
	}

	ctrl.Configure("INA228", *shunt, *maxcurrent)

	if *pol {
		ctrl.SetAlertPolarity("INA228", true)
	} else {
		ctrl.SetAlertPolarity("INA228", false)

	}

	ctrl.GetAlerts("INA228")

	if *sov > -1 {
		ctrl.SetShuntOverVoltage("INA228", *sov)
	}
	if *suv > -1 {
		ctrl.SetShuntUnderVoltage("INA228", *suv)
	}
	if *bov > -1 {
		ctrl.SetBusOverVoltage("INA228", *bov)
	}
	if *buv > -1 {
		ctrl.SetBusUnderVoltage("INA228", *buv)
	}
	if *maxtemp > -1 {
		ctrl.SetTemperatureLimit("INA228", *maxtemp)
	}
	if *maxpower > -1 {
		ctrl.SetPowerLimit("INA228", *maxpower)
	}

	Voltage := ctrl.GetVoltage("INA228")
	ShuntVoltage := ctrl.GetShuntVoltage("INA228")
	Current := ctrl.GetCurrent("INA228")
	Temperature := ctrl.GetTemperature("INA228")
	Power := ctrl.GetPower("INA228")
	log.Printf("Bus (Shunt) voltage = %.5v (%.5v) V, current = %.5v A, temperature = %.5v °C, power = %.5v W", Voltage, ShuntVoltage, Current, Temperature, Power)

	if *rbl {
		ctrl.ReadbackLimits("INA228")
	}
	//fmt.Println(strings.Repeat("-", width))

}
