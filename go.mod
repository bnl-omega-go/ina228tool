module BNL.gov/Exec

go 1.17

require (
	gitlab.cern.ch/bnl-omega-go/slowcontrol v0.0.0-20220418125943-4b9989df47a6
	golang.org/x/crypto v0.0.0-20220321153916-2c7772ba3064
)

require (
	github.com/codehardt/mmap-go v1.0.1 // indirect
	gitlab.cern.ch/bnl-omega-go/axi v0.0.0-20220302191738-5c72f24bd8f2 // indirect
	golang.org/x/sys v0.0.0-20220406163625-3f8b81556e12 // indirect
	golang.org/x/term v0.0.0-20201126162022-7de9c90e9dd1 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
	periph.io/x/conn/v3 v3.6.10 // indirect
	periph.io/x/host/v3 v3.7.2 // indirect
)
